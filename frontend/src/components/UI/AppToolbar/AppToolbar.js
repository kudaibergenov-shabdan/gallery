import React from 'react';
import {AppBar, Button, Grid, Link, makeStyles, Toolbar, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {Link as LinkRouter, Link as RouterLink} from "react-router-dom";
import {logoutUser} from "../../../store/actions/usersActions";

const useStyles = makeStyles(theme => ({
    mainLink: {
        color: "inherit",
        textDecoration: 'none',
        '$:hover': {
            color: 'inherit'
        }
    },
    staticToolbar: {
        marginBottom: theme.spacing(2)
    },
    link: {
        margin: '2px',
        color: 'white',
    },
}));

const AppToolbar = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);

    return (
        <>
            <AppBar position="fixed">
                <Toolbar>
                    <Grid container justifyContent="space-between" alignContent="center">
                        <Grid item>
                            <Typography variant="h6">
                                <LinkRouter to="/" className={classes.mainLink}>Photo gallery</LinkRouter>
                            </Typography>
                        </Grid>
                        <Grid item>
                            {user
                                ?
                                (
                                    <Grid container spacing={2} alignItems="center">
                                        <Grid item>
                                            Hello,
                                            <Link
                                                className={classes.link}
                                                component={RouterLink}
                                                variant="body2"
                                                to={`/users/${user._id}`}
                                            >
                                                {user.displayName}
                                            </Link>!
                                        </Grid>
                                        <Grid item>
                                            <Button
                                                variant="contained"
                                                color="default"
                                                onClick={() => {
                                                    dispatch(logoutUser())
                                                }}
                                            >
                                                Logout
                                            </Button>
                                        </Grid>
                                    </Grid>
                                )
                                :
                                (
                                    <>
                                        <Button component={LinkRouter} to="/register" color="inherit">Sign up</Button>
                                        <Button component={LinkRouter} to="/login" color="inherit">Sign in</Button>
                                    </>
                                )
                            }
                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>
            <Toolbar className={classes.staticToolbar}/>
        </>
    );
};

export default AppToolbar;