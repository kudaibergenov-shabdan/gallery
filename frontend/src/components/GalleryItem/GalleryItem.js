import React from 'react';
import {Button, Card, CardActions, CardContent, CardMedia, Grid, Link, makeStyles} from "@material-ui/core";
import imageNotAvailable from '../../assets/images/not_available.png';
import {apiURL} from "../../config";
import {useDispatch, useSelector} from "react-redux";
import {dropGalleryItem, setModalOpen} from "../../store/actions/galleryActions";
import {Link as RouterLink} from "react-router-dom";

const useStyles = makeStyles(theme => ({
    card: {
        height: '100%',
    },
    media: {
        textAlign: 'center',
    },
    mediaImage: {
        maxWidth: '200px',
        minHeight: '150px',
        height: 'auto',
    },
    link: {
        margin: '2px',
    }
}));

const GalleryItem = ({id, title, image, author}) => {
    const classes = useStyles();
    const user = useSelector(state => state.users.user);

    const dispatch = useDispatch();

    let cardImage = imageNotAvailable;
    if (image) {
        const pathToImage = apiURL + '/';
        cardImage = pathToImage + image;
    }

    const deleteGalleryItem = (galleryItemId) => {
        dispatch(dropGalleryItem(galleryItemId));
    }

    const handleModal = (imgSrc) => {
        dispatch(setModalOpen({isOpen: true, imgSrc}));
    }

    return (
        <Grid container item xs={3} >
            <Card className={classes.card}>
                <CardMedia className={classes.media}>
                    <div onClick={() => handleModal(cardImage)}>
                        <img src={cardImage} alt={title} className={classes.mediaImage}/>
                    </div>
                </CardMedia>
                <CardContent>
                    {title} by
                    <Link
                        className={classes.link}
                        component={RouterLink}
                        variant="body2"
                        to={`/users/${author._id}`}
                    >
                        {author.displayName}
                    </Link>
                </CardContent>
                {user?._id == author._id && (
                    <CardActions>
                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={() => deleteGalleryItem(id)}
                        >
                            Delete
                        </Button>
                    </CardActions>
                )}
            </Card>
        </Grid>
    );
};

export default GalleryItem;