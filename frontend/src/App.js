import {Route, Switch} from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import Login from "./containers/Login/Login";
import Main from "./containers/Main/Main";
import Register from "./containers/Register/Register";
import NewGalleryItem from "./containers/NewGalleryItem/NewGalleryItem";

const App = () => (
    <Layout>
        <Switch>
            <Route path="/" exact component={Main}/>
            <Route path="/users/:id" component={Main}/>
            <Route path="/users" component={NewGalleryItem}/>
            <Route path="/register" component={Register}/>
            <Route path="/login" component={Login}/>
        </Switch>
    </Layout>
);

export default App;
