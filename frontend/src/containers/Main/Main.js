import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";
import {Button, CircularProgress, Grid, Typography} from "@material-ui/core";
import GalleryItem from "../../components/GalleryItem/GalleryItem";
import {fetchGallery, setModalOpen} from "../../store/actions/galleryActions";
import Modal from "../../components/UI/Modal/Modal";

const Main = ({match}) => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const gallery = useSelector(state => state.gallery.gallery);
    const fetchLoading = useSelector(state => state.gallery.fetchLoading);
    const showModalisOpen = useSelector(state => state.gallery.showModal.isOpen);
    const showModalimgSrc = useSelector(state => state.gallery.showModal.imgSrc);

    useEffect(() => {
        if (match.params) {
            dispatch(fetchGallery(match.params.id));
        } else {
            dispatch(fetchGallery());
        }
    }, [dispatch, match.params.id]);

    const closeModal = () => {
        dispatch(setModalOpen({isOpen: false}));
    }

    return (
        <>
            <Modal show={showModalisOpen} close={showModalisOpen}>
                <img src={showModalimgSrc} alt="No image" width="100%" height="auto"/>
                <Grid container direction="column" alignItems="center">
                    <Button onClick={closeModal}>[X]</Button>
                </Grid>
            </Modal>

            <Grid container direction="column" spacing={2}>
                <Grid item container justifyContent="space-between" alignItems="center">
                    <Grid item>
                        {
                            user ? (
                                <Typography variant="h4">{user.displayName}'s Gallery</Typography>
                            ) : (
                                <Typography variant="h4">Gallery</Typography>
                            )
                        }
                    </Grid>

                    {user && (
                        <Grid item>
                            <Button
                                color="primary"
                                variant="contained"
                                component={Link}
                                to="/users"
                            >Add
                            </Button>
                        </Grid>
                    )}
                </Grid>
                <Grid item>
                    <Grid item container justifyContent="center" direction="row" spacing={1}>
                        {fetchLoading ? (
                            <Grid container justifyContent="center" alignItems="center">
                                <Grid item>
                                    <CircularProgress/>
                                </Grid>
                            </Grid>
                        ) : (
                            gallery.map(galleryItem => (
                                <GalleryItem
                                    key={galleryItem._id}
                                    id={galleryItem._id}
                                    title={galleryItem.title}
                                    image={galleryItem.image}
                                    author={galleryItem.author}
                                />
                            ))
                        )}
                    </Grid>
                </Grid>
            </Grid>
        </>
    );
};

export default Main;