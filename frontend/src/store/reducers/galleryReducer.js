import {
    CLEAR_ERROR_ITEM,
    CREATE_GALLERY_ITEM_FAILURE,
    CREATE_GALLERY_ITEM_REQUEST, CREATE_GALLERY_ITEM_SUCCESS,
    FETCH_FAILURE_GALLERY,
    FETCH_REQUEST_GALLERY,
    FETCH_SUCCESS_GALLERY, SET_MODAL_OPEN
} from "../actions/galleryActions";

const initialState = {
    gallery: [],
    fetchLoading: false,
    createGalleryItemLoading: false,
    createGalleryItemError: null,
    showModal: false,
};

const galleryProducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_REQUEST_GALLERY:
            return {...state, fetchLoading: true};
        case FETCH_SUCCESS_GALLERY:
            return {...state, fetchLoading: false, gallery: action.payload};
        case FETCH_FAILURE_GALLERY:
            return {...state, fetchLoading: false};
        case CREATE_GALLERY_ITEM_REQUEST:
            return {...state, createGalleryItemLoading: true};
        case CREATE_GALLERY_ITEM_SUCCESS:
            return {...state, createGalleryItemLoading: false, createGalleryItemError: null};
        case CREATE_GALLERY_ITEM_FAILURE:
            return {...state, createGalleryItemLoading: false, createGalleryItemError: action.payload};
        case SET_MODAL_OPEN:
            return {
                ...state, showModal: action.payload
            }
        case CLEAR_ERROR_ITEM:
            return {...state, createGalleryItemError: null};
        default:
            return state;
    }
};

export default galleryProducer;