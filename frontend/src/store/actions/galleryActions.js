import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import WarningIcon from '@material-ui/icons/Warning';
import {historyPush} from "./historyActions";

export const FETCH_REQUEST_GALLERY = 'FETCH_REQUEST_GALLERY';
export const FETCH_SUCCESS_GALLERY = 'FETCH_SUCCESS_GALLERY';
export const FETCH_FAILURE_GALLERY = 'FETCH_FAILURE_GALLERY';

export const CREATE_GALLERY_ITEM_REQUEST = 'CREATE_GALLERY_ITEM_REQUEST';
export const CREATE_GALLERY_ITEM_SUCCESS = 'CREATE_GALLERY_ITEM_SUCCESS';
export const CREATE_GALLERY_ITEM_FAILURE = 'CREATE_GALLERY_ITEM_FAILURE';

export const SET_MODAL_OPEN = 'SET_MODAL_OPEN';

export const CLEAR_ERROR_ITEM = 'CLEAR_ERROR_ITEM';

export const fetchRequestGallery = () => ({type: FETCH_REQUEST_GALLERY});
export const fetchSuccessGallery = gallery => ({type: FETCH_SUCCESS_GALLERY, payload: gallery});
export const fetchFailureGallery = error => ({type: FETCH_FAILURE_GALLERY, payload: error});

export const createGalleryItemRequest = () => ({type: CREATE_GALLERY_ITEM_REQUEST});
export const createGalleryItemSuccess = () => ({type: CREATE_GALLERY_ITEM_SUCCESS});
export const createGalleryItemFailure = (error) => ({type: CREATE_GALLERY_ITEM_FAILURE, payload: error});

export const setModalOpen = img => ({type: SET_MODAL_OPEN, payload: img});

export const clearErrorItem = () => ({type: CLEAR_ERROR_ITEM});

export const fetchGallery = (authorId) => {
  return async dispatch => {
      try {
          dispatch(fetchRequestGallery);
          let response;
          if (authorId) {
              response = await axiosApi.get(`/users/${authorId}`);
          } else {
              response = await axiosApi.get('/galleries');
          }
          dispatch(fetchSuccessGallery(response.data));
      }
      catch (error) {
          dispatch(fetchFailureGallery());
          toast.error('Could not fetch galleries!', {
              position: toast.POSITION.BOTTOM_RIGHT,
              theme: 'colored',
              icon: <WarningIcon/>
          });
      }
  };
};

export const dropGalleryItem = galleryItemId => {
    return async dispatch => {
        try {
            await axiosApi.delete(`/galleries/${galleryItemId}`);
            dispatch(fetchGallery());
            toast.success('Item deleted', {
                position: toast.POSITION.BOTTOM_RIGHT,
            });
        }
        catch (error) {
            toast.error('Something went wrong while deletion', {
                position: toast.POSITION.BOTTOM_RIGHT,
            });
        }
    }
};

export const createGalleryItem = item => {
    return async dispatch => {
        try {
            dispatch(createGalleryItemRequest());
            await axiosApi.post('/galleries', item);
            dispatch(createGalleryItemSuccess());
            dispatch(historyPush('/'));
            toast.success('Item created', {
                position: toast.POSITION.BOTTOM_RIGHT,
            });
        } catch (error) {
            dispatch(createGalleryItemFailure(error.response.data));
            toast.error('Could not create item', {
                position: toast.POSITION.BOTTOM_RIGHT,
            });
        }
    };
};