import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunk from "redux-thunk";
import {Provider} from "react-redux";

import history from "./history";

import usersReducer, {initialState} from "./store/reducers/usersReducer";
import galleryReducer from "./store/reducers/galleryReducer";

import {MuiThemeProvider} from "@material-ui/core";
import theme from "./theme";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {Router} from "react-router-dom";
import axiosApi from "./axiosApi";
import {loadFromLocalStorage, saveToLocalStorage} from "./store/localStorage";


const rootReducer = combineReducers({
    'users': usersReducer,
    'gallery': galleryReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();

const store = createStore(
    rootReducer,
    persistedState,
    composeEnhancers(applyMiddleware(thunk))
);

store.subscribe(() => {
    saveToLocalStorage({
        users: {
            ...initialState,
            user: store.getState().users.user,
        }
    });
});

axiosApi.interceptors.request.use(config => {
    try {
        config.headers['Authorization'] = store.getState().users.user.token
    } catch (e) {
    }

    return config;
});

axiosApi.interceptors.response.use(res => res, e => {
    if (!e.response) {
        e.response = {data: {global: 'No internet'}};
    }

    throw e;
});

const app = (
    <Provider store={store}>
        <Router history={history}>
            <MuiThemeProvider theme={theme}>
                <ToastContainer/>
                <App/>
            </MuiThemeProvider>
        </Router>
    </Provider>
)

ReactDOM.render(app, document.getElementById('root'));
