const express = require('express');
const config = require('../config');
const path = require("path");
const multer = require("multer");
const {nanoid} = require("nanoid");
const Gallery = require('../models/Gallery');
const auth = require("../middleware/auth");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const galleries = await Gallery.find().populate('author', 'displayName');
        res.send(galleries);
    } catch (error) {
        res.sendStatus(500);
    }
});

router.post('/', auth, upload.single('image'), async (req, res) => {
    try {
        const galleryData = {
            title: req.body.title,
            author: req.user,
        };

        if (req.file) {
            galleryData.image = 'uploads/' + req.file.filename;
        }

        const gallery = new Gallery(galleryData);
        await gallery.save();
        res.send(gallery);
    } catch (error) {
        res.status(400).send(error);
    }
});

router.delete('/:id', auth, async (req, res) => {
    try {
        const galleryItem = await Gallery.findById(req.params.id);
        if (!galleryItem) {
            return res.status(400).send({error: 'No such galleryItem'});
        }

        const galleryItemByUser = await Gallery.findOne({"_id": req.params.id, "author": req.user});
        if (!galleryItemByUser) {
            return res.status(403).send({error: 'You have not permission to delete this gallery item'});
        }

        const deletedGalleryItem = await Gallery.findByIdAndDelete(req.params.id);
        if (deletedGalleryItem) {
            res.send(`Item ${deletedGalleryItem.title} removed`);
        } else {
            res.status(404).send({error: 'Gallery not found'});
        }
    } catch (error) {
        res.sendStatus(500);
    }
});

module.exports = router;