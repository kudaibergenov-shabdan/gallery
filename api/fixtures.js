const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Gallery = require("./models/Gallery");

const run = async () => {
    await mongoose.connect(config.db.url);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [admin, user] = await User.create({
        email: 'admin@gmail.com',
        password: '123',
        token: nanoid(),
        displayName: 'Admin',
    }, {
        email: 'user@gmail.com',
        password: '123',
        token: nanoid(),
        displayName: 'User',
    });

    await Gallery.create({
            title: 'Beautiful nature',
            author: admin,
            image: 'fixtures/beautiful_nature.jpeg'
        }, {
            title: 'Парящие горы',
            author: admin,
            image: 'fixtures/flying_mountains.jpg',
        }, {
            title: 'Сказочная зима',
            author: admin,
            image: 'fixtures/fabulous_winter.jpg',
        }, {
            title: 'Пустыня',
            author: admin,
            image: 'fixtures/desert.jpg'
        }, {
            title: 'Песчанная буря',
            author: user,
            image: 'fixtures/sandstorm.jpg',
        }, {
            title: 'Мать героиня',
            author: user,
            image: 'fixtures/mother_heroine.jpg',
        }, {
            title: 'Снова в школу',
            author: user,
            image: 'fixtures/back_to_school.jpg',
        },
    );

    await mongoose.connection.close();
};

run().catch(console.error);