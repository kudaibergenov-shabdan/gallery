const mongoose = require('mongoose');

const GallerySchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        validate: [
            {
                validator: async function (value) {
                    if (value) {
                        const isGalleryTitleFound = await Gallery.findOne({
                            author: this.author,
                            title: value
                        });
                        if (isGalleryTitleFound)
                            return false
                    }
                },
                message: 'Such gallery title is already existed !',
            }
        ]
    },
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    image: String,
});

const Gallery = mongoose.model('Gallery', GallerySchema);

module.exports = Gallery;
